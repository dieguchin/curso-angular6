import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    selected: boolean;
    servicios: string[];
    id = uuid();

    constructor(public nombre: String, public imagenUrl: String, public votes: number = 0) {
        this.servicios = ['Pileta', 'Desayuno'];
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
    isSelected() {
        return this.selected;
    }
    voteUp() {
        this.votes++;
    }
    voteDown() {
        this.votes--;
    }
}
